
public class Vertex<T> {

	private T data;
	
	public Vertex(T data) {
		this.data = data;
	}
	
	public T getData() {
		return data;
	}
	
	public void setData(T data) {
		this.data = data;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		
		if (!(o instanceof Vertex)) {
			return false;
		}
		
		Vertex<T> v = (Vertex<T>) o;
		
		return v.getData().equals(this.data);
	}
	
	@Override
	public int hashCode() {
		int result = 1;
		int primeNumber = 37;
		int propertyHashSum = 0;
		
		propertyHashSum += data == null ? 0 : data.hashCode();
		
		return (primeNumber * result) + propertyHashSum;
	}
}
