import java.util.Set;

public class Driver {
	public static void main(String[] args) {
		Graph<String> graph = new Graph<String>();
		
		graph.addVertex("A");
		graph.addVertex("B");
		graph.addVertex("C");
		graph.addVertex("D");
		graph.addVertex("E");
		
		graph.addUndirectedEdge("A", "B");
		graph.addUndirectedEdge("A", "C");
		graph.addUndirectedEdge("B", "D");
		graph.addUndirectedEdge("C", "D");
		graph.addUndirectedEdge("D", "E");
		
		Set<String> dfs = graph.DFS("A");
		System.out.println(dfs);
		
		Set<String> bfs = graph.BFS("A");
		System.out.println(bfs);
	}
}
