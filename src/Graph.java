import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

/*
 * Graph implementation with the adjacency list approach.
 */
public class Graph<T> {
	
	private Map<Vertex<T>, List<Vertex<T>>> graph;
	
	public Graph() {
		graph = new HashMap<Vertex<T>, List<Vertex<T>>>();
	}
	
	// O(1)
	public void addVertex(T data) {
		graph.putIfAbsent(new Vertex<T>(data), new LinkedList<Vertex<T>>());
	}
	
	// O(|E|) - ?
	public void removeVertex(T data) {
		Vertex<T> vertex = new Vertex<T>(data);
		
		java.util.Iterator<Entry<Vertex<T>, List<Vertex<T>>>> iterator = graph.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Vertex<T>, List<Vertex<T>>> entry = iterator.next();
			
			entry.getValue().remove(vertex);
		}
		
		graph.remove(vertex);
	}
	
	// O(1)
	public void addUndirectedEdge(T data1, T data2) {
		Vertex<T> vertex1 = new Vertex<T>(data1);
		Vertex<T> vertex2 = new Vertex<T>(data2);
		
		if (!graph.containsKey(vertex1) || !graph.containsKey(vertex2)) {
			throw new IllegalArgumentException("One or more of the provided vertices do not exist in the graph. Consider using the addVertex(vertex) before adding an edge.");
		}
		
		graph.get(vertex1).add(vertex2);
		graph.get(vertex2).add(vertex1);
	}
	
	// O(|V|) - ?
	public void removeUndirectedEdge(T data1, T data2) {
		
		Vertex<T> vertex1 = new Vertex<T>(data1);
		Vertex<T> vertex2 = new Vertex<T>(data2);
		
		List<Vertex<T>> vertex1AdjacentList = graph.get(vertex1);
		List<Vertex<T>> vertex2AdjacentList = graph.get(vertex2);
		
		if (vertex1AdjacentList == null || vertex2AdjacentList == null) {
			// There is no undirected graph between vertex 1 and vertex 2.
			return;
		}
		
		vertex1AdjacentList.remove(vertex2);
		vertex2AdjacentList.remove(vertex1);
	}
	
	public Set<T> DFS(T root) {
		
		Set<T> visited = new LinkedHashSet<T>();
		Vertex<T> rootVertex = new Vertex<T>(root);
		
		if (!graph.containsKey(rootVertex)) {
			return null;
		}
		
		Stack<Vertex<T>> stack = new Stack<Vertex<T>>();
		stack.push(rootVertex);
		
		while (!stack.isEmpty()) {
			Vertex<T> currentVertex = stack.pop();
			
			if (!visited.contains(currentVertex.getData())) {
				visited.add(currentVertex.getData());
				
				for (Vertex<T> vertex : graph.get(currentVertex)) {
					if (!visited.contains(vertex.getData())) {
						stack.push(vertex);
					}
				}
			}
		}
		
		return visited;
	}
	
	public Set<T> BFS(T root) {
		
		Vertex<T> rootVertex = new Vertex<T>(root);
		if (!graph.containsKey(rootVertex)) {
			return null;
		}
		
		Set<T> visited = new LinkedHashSet<T>();
		Queue<Vertex<T>> queue = new LinkedList<Vertex<T>>();
		queue.add(rootVertex);
		
		while (!queue.isEmpty()) {
			Vertex<T> currentVertex = queue.poll();
			
			if (!visited.contains(currentVertex.getData())) {
				visited.add(currentVertex.getData());
				
				for (Vertex<T> vertex : graph.get(currentVertex)) {
					if (!visited.contains(vertex.getData())) {
						queue.add(vertex);
					}
				}
			}
		}
		
		return visited;
	}
}
